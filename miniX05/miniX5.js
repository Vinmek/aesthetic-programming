/*
The rules I have set up for my program is as follows:
  - The canvas is rotated counter clockvise by one degree every frame.
  - A triangle is drawn every frame. At an initial distance of 10 from the
    center. Until there is a total number of 4500 triangles.
  - For every Triangle drawn the following will be drawn 1 degree in
    a clockvise direction
  - Every frame the trangles distance to the center is increased by 0.05.
  - Every 10 frames the triagles distance to each other is increased by 0.05.
*/
//The usual amount of variables
let gridDist;
let arrows = []; //Makes an empty array to store objects.
let a;
let vinkel; let tornado;
let originX; let originY;

function setup() {
  createCanvas(windowWidth,windowHeight); //Creates canvas
  frameRate(30); //Sets frameRate to 30 to make it easier to look at.
  gridDist = 0.05; //The increase in distance on both the x and y axis.
  originX = 10; originY = 10; //Makes it so we get the eye of the storm.
  vinkel = 0; tornado = 0; //The two rotational variables.
  a = 0; //This variable creates new indexvalues every execution.
}

function draw() {
  background(0);
  translate(windowWidth/2,windowHeight/2); //Changes Origin.
  angleMode(DEGREES); //Sets angle calculation to degrees.
  rotate(tornado) //Rotates the figure.
  tornado--; //Makes the rotation go counterclockwise.

if(a < 4500) { //Sets a cap on triangles genrated.
  push(); //Isolates this rotation form the other one. Is required else no rotation.
  arrows[a] = new Arrow(originX,originY, vinkel); //Creates a new object.
  a++; //Increases the index value inside the array which makes room for new objects.
  vinkel++; //Increases angle.
  originX+=gridDist; //Makes the storm grow larger en diameter.
  if(originX > 10) {
    originY+=gridDist; //This creates the feeling of a spiral pattern.
  }                  //It also is responsible for the later splits.
  pop()
}
for(let n of arrows) { //This applys the function with n to all versions of arrow.
    n.show();
    n.spin();
  }
}

//This part is more complicated. I made an object to stand in for any of
//the arrows in the figure. It actually do have an effect on the way they rotate.
//If not for the objects the figure would just be a normal spiral.
class Arrow { //The idea is that this object contains the rules for every triangle.
  constructor(_x,_y,_sp) { //The constructor translates external inputs.
    this.x = _x;
    this.y = _y;
    this.sp = _sp;
  }
  show() { //I use the vertex syntax to draw the triangles
    beginShape(); //I define the vertexes in relation to the center.
    vertex(this.x - 8, this.y);
    vertex(this.x + 8, this.y - 5);
    vertex(this.x + 8, this.y + 5);
    endShape();
  }
  spin() {
    //This was supposed to spin the individual triangle.
    //It didn't work so instead it now changes the position of the triangles drawn.
    angleMode(DEGREES);
    rotate(this.sp);
  }
}
