### WARNING
This program contains spinning elements and may cause vertigo and nausea.
If you have difficulties with spinning images watch with caution.

Link to RUNME - https://vinmek.gitlab.io/aesthetic-programming/miniX05/indexX5

Link to code - https://gitlab.com/Vinmek/aesthetic-programming/-/blob/main/miniX05/miniX5.js

## Storm - miniX5
I have chosen to call my generative artwork for storm. Because it resembles the
formation of clouds in a hurricane or tornado.

My sketch runs on the following 5 rules:
- The canvas is rotated counter clockvise by one degree every frame.
- A triangle is drawn every frame. At an initial distance of 10 from the
  center. Until there is a total number of 4500 triangles.
- For every Triangle drawn the following will be drawn 1 degree in
  a clockvise direction
- Every frame the trangles distance to the center is increased by 0.05.
- Every 10 frames the triagles distance to each other is increased by 0.05.

These rules make it so the sketch makes a continually spinning set of triangles
that spiral our from the center

![](Billede_miniX5_1.png)

The sketch shows a progression through different patterns when running. It
starts out by rotating quickly around the center slowly moving outward. The
next faze is the spiral, that is slowly broken up when the program begins
making columns outward.

![](Billede_miniX5_2.png)

![](Billede_miniX5_3.png)

This happens due to the increase in the increase to the y position of each
triangle.

Another interesting factor of the program is, that I have made use of objects
to create the individual triangles. I do this because something in the code
is broken and creates a different pattern when the triangles are created as
part of a function.

### A conversation with the materials
I found through working with my sketch, that I often were unable to predict how
certain changes would affect the end result. I found that the process became a
back and forth between myself and the computer. This has made me rethink how
the work relationship between us where situated.

The relationship can to me be explained in two ways. The first is like athletes
in a relay constantly running a distance and passing the baton on to the next.
I would do my thing and pass the project onto the program, that would answer
with a result that I could the work out from.

The process ended up resembling what Donald A. Schön would call a "reflective
conversation with the materials". In this I would argue that the program
becomes the materials that I work with, like how a director works with the
actors they work with.

Another interpretation of the dynamic between the human and computer could be
that of an artist and a museum curator. Where the program is the one creating
art the human is the one who judges, commission and decides what is to be put
on display. Knowing something about art, but not being the actual creator of it.

### The pursuit of the random
My sketch is not Random, it is entirely deterministic, yet at a glance it
looks random. The further out the spiral goes the more arbitrary the placement
of each triangle, but it is actually really precise. The program just works in
such a quick fashion that we cannot comprehend or predict where the next
triangle is going to be placed.

I would nonetheless like to write a bit about the concept of randomness.
Why is randomness so alluring to us? It is said that humans fear death as
well as the dark, because these parts of our lives is shrouded in the unknown.
We have no idea what might be on the other side, which is frightening.
In my opinion randomness is like this the uncertainty of outcome becomes the
unknown, but even so we do not fear randomness we embrace it seek it out even.

In the book *10 PRINT CHR$(205.5+RND(1))* randomness gets described as
involving: "The masochistic interplay between pleasure and pain".
Somehow humans are drawn to uncertainty, but for what reason. Something about
the unknown is at the same time frightening and devilishly alluring.
We do things to get a peak into the void of the unknown, maybe just to watch
or maybe in an attempt to conquer it and through that feel safe.
Higher stakes games of chance takes this to an extreme such as with Russian
Roulette. By gambling with their lives. The players are drawn ever closer to
this void of the unknown.

My conclusion here is that randomness is so tantalizing because it is pathway
that connects the participant to what I call the void of the unknown.
The randomness doesn't even need to be real, it can be an imitation of
randomness or pseudorandom, the illusion seems to be enough for many.
