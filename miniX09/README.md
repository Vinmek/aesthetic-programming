Links to the individual flowcharts:

[Simon's miniX6 flowchart](https://gitlab.com/Vikingboi/aestheticprogramming/-/blob/main/miniX9/Simon_flowchart.jpg)

[Asbjørn's miniX7 flowchart](https://gitlab.com/asbjrnahle33/aestheticprogramming/-/blob/main/miniX9/Flowchart.PNG)

[Mikkel's miniX4 flowchart](https://gitlab.com/Vinmek/aesthetic-programming/-/blob/main/miniX09/Assets/Mikkel_miniX4_flowchart.png)

[Naja's miniX5 flowchart](https://gitlab.com/najadigitaldesign/aestetisk-programmering/-/blob/main/miniX9/Naja_-_miniX5_Flowchart.drawio.png)



## **Explanation of the two ideas**

**Life Planner:** <br>
A program that asks for your information and then gives you some horoscope news and tracks your mood in a calendar, but the program gets more and more disturbing with the data it asks for and shows. Therefore making a quite innocent and later on disturbing program. <br>
The two main aspects are surveillance capitalism and one's own dignity and determinism. <br>
![](Assets/miniX9_LifePlanner_flowchart.png) <br>
This idea came from the modern tendency to log and track many aspects of life on an app. We wanted to take that to an extreme to criticise having a program slowly take over very important decisions, when in the beginning, it was just an innocent tracking program to mirror your answers back to you.

_Quote of inspiration:_ "This line of critique argues that we are,
through datafication, becoming dependent on (external, privatised) data measurements to tell
us who we are, what we are feeling, and what we should be doing, which challenges our basic
conception of human agency and knowledge." (Mejias & Couldry, 2019)

**Ditritivore:** <br>
A program that can “eat" old programs that haven't been used for a long time. <br>
The idea behind this program is the question of why data and software needs to be permanent. <br>
![](Assets/miniX9_Ditritivore_flowchart.png)

This idea would call into question the way data is used and stored, because data that isn’t being used will eventually “die” and get broken down. This would change the way of storing big data, as measures would have to be put in place for businesses to keep their data alive.

The main reason we have for wanting to pursue this, is to take a critical stand towards the insistence on storing things for eternity.

The name comes from the Latin word “detritivore”, which refers to organisms that eat things by decomposing them. <br> We named our idea **Di**tritivore to refer to the word “digital”. <br>

_Quote of inspiration:_ "**Under late capitalism, temporality itself seems to have been captured,** and “there is a relentless incursion of the non-time of 24/7 into every aspect of social or personal life. There are, for example, almost no circumstances now that cannot be recorded or archived as digital imagery or information.”" (Soon & Cox, 2020) We twist this to focus on the forever archived digital imagery and information, focusing especially on the first part of the quote.

---
<br>

**What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?** <br>
It was difficult in the beginning, because we did not yet know all the details of the program (and still do not entirely), so the Life Planner suddenly became very complex to cooperate around. The flowchart of that idea ended up in a simplified version, which does not explain all the procedures, but rather the concept and what is happening. The Ditritivore flowchart is also conceptual.

**What are the technical challenges facing the two ideas and how are you going to address these?** <br>
Life Planner: <br>
It will be hard to decide the pathways each answer will lead to. How to connect the answers that will give the output dependent on those answers. For example, what answers the user has to give to get the output “You should not see Mark anymore.”
Note: Though our flowchart runs over a month, our code is not going to be running that long. We are going to show the concept over a short time.

Ditritivore: <br>
It will be a challenge to see if we can make the part of the code that determines if the object is old enough to “eat” work. Also a mechanism for checking the age of a certain piece of data is a mechanical problem we would have to solve for the program to work.
Another challenge for this work is to communicate the process as this program is not flashy or grand but subdued in its expression.

**In which ways are the individual and the group flowcharts you produced useful?** <br>
The flowcharts are really useful in its way to project one's mind and mindset to others.

The individual one is great for your own sake and great at setting yourself up to date with how you would see and how others will see perspective. It also warms up your flowchart-muscles a bit before cooperating.

On the group side. The group flowcharts are great for people to be on the same page at the same time. It is also a great way for making a general plan for a longer time period. Therefore also making a goal in the meantime.
We found that having to draw out the idea physically quickly manifested the areas where the individual members of our group had differing visions about how the programs would function and work.

---

**References** <br>
Mejias, Ulises A. and Nick Couldry. "Datafication". Internet Policy Review 8.4 (2019). Web. 16 Feb. 2021. https://policyreview.info/concepts/datafication <br>
Soon Winnie & Cox, Geoff, "Data capture", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020.
