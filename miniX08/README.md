#### Group 1: Naja, Asbjørn, Simon and Mikkel

[RunMe](https://vinmek.gitlab.io/aesthetic-programming/miniX08/IndexX8) <br>
[source code](https://gitlab.com/Vinmek/aesthetic-programming/-/blob/main/miniX08/miniX8.js) <br>

![](Assets/Gif_miniX8.gif)

Alternative titles: &nbsp; I Am &nbsp; &nbsp; &nbsp; &nbsp; What am i? &nbsp; &nbsp; &nbsp; &nbsp; To Am or Not To Am &nbsp; &nbsp; &nbsp; &nbsp; Identity Wall

Official title:
_**What** 01010111 01101000 01100001 01110100 **Am**  01100001 01101101 **I** 01101001 **?** 00111111_

Figuring out one’s identity can feel like a spaghetti bowl of options, so what if programs, who are isolated to their own code, had their own limited understanding of the self?
What Am I? explores this by showcasing a program trying to define itself by what it is and what it is not, and to see the thinking progress. Furthermore, the code is written in code work to try and show how the program’s thinking process and make it seem more existential to the reader.

In our project, we started off by taking some inspiration from our Zach Whalen’s programming of the 1967 poem A House of Dust (Knowles & Tenney), where a poem is generated with randomly selected words. Our program is also inspired by 10PRINT (Montfort et al, 2012) with the words being regenerated over and over across the screen until it is covered.
We thought of our project (miniX8) as an artificial intelligence learning what it is. In the process of the AI figuring out what it is, it has a hard time doing so. In our vocable code, you can read directly from the source, how the computer thinks and how it manages to go through different stages of thinking. In these stages it “thinks” about: sentience, identity crisis, thinking, brain capacity, desperation, aspects of one's mind, aspects of what it is, its own aspects and rethinking.

We also made it so you could read out the code in the script. Therefore making you able to read the code as a non-coder person. By doing so, we have then made it so that the computer writes down its thought process, so it is easily available to read its narrative. The title of our ReadMe is also well thought out. We made it so that instead of making it easily available for humans, we thought of translating the title to a binary code, so that the computer is the one that could read it, and therefore making it more the program’s project instead of ours. We ended up mixing the binary code (we wrote the translation before each binary-word) and English words, as to show the mix between the computer’s “thoughts” and how accessible those thoughts are to us humans because of codework - therefore making the best of both worlds, a real Hannah Montana moment. <br>

![](Assets/Picture_miniX8_1.png)

**Describe how your program works, and what syntax you have used, and learnt? <br>**
As stated before our program works in a similar way as to the 10PRINT program. The program collects a random string from the json file named Ego. This file contains
about 90 adjectives ranging from positive to negative. The program uses these strings and progressively writes lines switching between the statements “I Am Adjective”, “Not Adjective”. Each frame the program adds to the x and y value which makes a pattern of sentences. This small pattern repeats over and over and fills out the screen. Once the screen is full the program resets and draws new phrases.

![](Assets/Picture_miniX8_2.png)

**Analyze your own e-lit work by using the text Vocable Code and/or The Aesthetics of Generative Code (or other texts that address code/voice/language). <br>**
In the text Vocable Code page 182 (Soon & Cox, 2020) it is discussed how programming has a language and bodily impact/relation to the user. In our project we have tried to make all the variables and names in the programing to something that can be read like a poem. This makes the code a lot more vocable so to speak. When people read code or any text we have an internal voice reading it aloud, this internal voice can read better and be used better when the code is written more like natural language instead of in binary.


**How would you reflect on your work in terms of Vocable Code? <br>**
We have made a project that gets a computer to seem more alive and even more human. This can be seen in the way that we have written the code so it can be read both by the machine, but it also works as a sort of poem for us humans. The imitation game is really on, and we made it so that there is a big variety of different ideas and options in the phrases, thereby making it more ‘human’.

----

References: <br>
Soon Winnie & Cox, Geoff, "Vocable Code", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 165-186 <br>
Whalen, “A House of Dust”, 2014, http://www.zachwhalen.net/pg/dust/ <br>
Nick Montfort et al. “Randomness,” 10 PRINT CHR$(205.5+RND(1)); : GOTO10, https://10print.org/ (Cambridge, MA: MIT Press, 2012), 119-146.
