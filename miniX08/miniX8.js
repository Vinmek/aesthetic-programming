let sentience; // the sentence
let mentalSpace;
let clarity;
let identityCrisis; // x
let thinking; // y
let brainCapacity; // endresult of sentence (t)
let desperation; // offset

// preloads the Json into the libary meanwhile filling the varible 'ofMy'

function preload() {
  ofMy = loadJSON('Ego.json');
}
/*
    Different varibles filled with numbers
    Creates a canvas with the width and height of set screen
    Sets background color to 0
    Sets framerate to 2

*/
function setup() {
  identityCrisis = 5;
  thinking = 10;
  brainCapacity = 0;
  desperation = 0;
  mentalSpace = windowHeight;
  clarity = windowWidth;
  createCanvas(clarity,mentalSpace);
  background(0);
  frameRate(2);
}
/*
    Fills the "aspects" varible with the defined preload .json file "ofMy" and searches in the folder called "mind"

    Fills the "ofWhat" varible with a random number from the aspects.length, therefore picking a word from the defined .json file
    The number is translated then to a integer, by rounding up or down to the nearest whole number. Therefore making it a real number instead of a decimal number.

    The next three varibles "some","someOther" and "myOwn" is then defined with the same code "aspects[ofWhat].iam;" therefore all of them are picking a random
    word from the .json file under the folder "mind", then "length" and at last "iam". Under "iam" is all of the 90 different words that can have a negative, positive and a neutrale setting

    In the last varible "sentience" is filled with some string that always writes the same + the definded varibles "some","someOther" and "myOwn" that all have their
    unique words randomly put into them from the .json list

*/
function being() {
  let aspects = ofMy.mind; //
  let ofWhat = int(random(aspects.length));
  let some = aspects[ofWhat].iam;
  let someOther = aspects[ofWhat].iam;
  let myOwn = aspects[ofWhat].iam;
  sentience = [
    "I Am " + some, "Not " + someOther,
      "I Am " + some,"Not " + myOwn
    ];
}
/*
    The syntax "translate" is used to update the program itself so it later in the process can reorginize its
    own offset position. This is done by translating its postion to the varible "desparation (x)" that currently is set to 0
    therfore putting the text to the "desparation (x)" coordinate

    "being" is then continuesly being repated

    "fill" is the color of text is set to the value 255 and turning white by doing so

    sets textsize to 15

    Sets the text to align with the left and then center

    Shows the text on screen from the varible "sentience" and chooses which sentence it should print based on its "brainCapacity (t)"
    then it chooses the x coordinate based on the "indentityCrisis (x)" and then the y coordinate based on "thinking (y)"

    Sets the "thinking (y)" varible to plus 20

    Plusses "brainCapacity (t)" with itself, thereby increasing (t)

*/

function draw() {
  translate(desperation,0);
  being();
    fill(255);
    textSize(15);
    textAlign(LEFT,CENTER);
    text(sentience[brainCapacity],identityCrisis,thinking)
    thinking += 20;
    brainCapacity++

    if(identityCrisis <= 15) { // The text goes one down, one to the right. makes an indented line.
      identityCrisis += 10;
    }
    if(thinking % 90 == 0) { // When the fourth line has been written, go 10 pixels down, move the word to the left again and start over.
      thinking += 10;
      identityCrisis = 5;
      brainCapacity = 0;
    }
    if(thinking > mentalSpace) { // mentalSpace = windowHeight. if it reaches the bottom of the screen, start new line of sentences.
      thinking = 10;
      desperation += 150
    }
    if(desperation > clarity) { // clarity = windowWidth. if the offset goes beyond the window, go to function rethinking();
      rethinking();
    }
}
function rethinking() { // it starts over at the beginning of the screen.
  clear();
  background(0);
  translate(-desperation,0); // -offset makes sure that it doesn't keep going off-screen. it jumps back to the left of the screen.
  identityCrisis = 5;
  thinking = 10;
  brainCapacity = 0;
  desperation = 0;
}



// This is a graveyard. Only the fool dare enter here.

// let frame = frameCount;
// if(frame % 4 == 3) { // huskeseddel: vi skal lave KÆMPE note omkring rest og brøker.
//   t = 1;
// } else if(frame % 4 == 0) {
//   t = 2;
// } else {
//   t = 0;
// }
