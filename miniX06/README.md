### Note:
The sprites used in this game was NOT created by me. I am only able to place
the origin of the 'Hero' sprite as a sprite from the game
*Fire Emblem Awakening*.

**I am not sure why, but the program seems to have troubles starting up.
It might be do to how complex it is.**

RUNME: https://vinmek.gitlab.io/aesthetic-programming/miniX06/IndexX6

Links to the different js files.

Main Code: https://gitlab.com/Vinmek/aesthetic-programming/-/blob/main/miniX06/miniX6.js

Entity Code: https://gitlab.com/Vinmek/aesthetic-programming/-/blob/main/miniX06/miniX6_Entity.js

Wall Code: https://gitlab.com/Vinmek/aesthetic-programming/-/blob/main/miniX06/miniX6_Wall.js

## Object abstraction miniX6
### Skulls & Sorcerers
I have chosen to name my game Skulls and Sorcerers. The name might not
perfectly reflect, what a player will experience playing the game, as it
only is representing the player and monsters sprites that I have used.

The game is simple. You have one objective, to get through the randomly
generated maze and escape. In your way is a progressively growing hoard of
skeletons, that try to hinder you.

![](Gif_miniX6_1.gif)

The game consist of two types of objects Entities and Barriers. The
barriers make up the walls and the entities make up all the moving
creatures within. When the player tries to get through the maze an in
game timer triggers new skeletons to appear.
### The objects
My objects are as stated separated in two categories. One is far more
complex than the other. So I will focus on the entity class more than the
barrier class.
#### Barriers
I will quickly sum up the barriers. The barriers are defined by their
position and dimensions. They are only able to show themselves.
#### Entities
Far more interesting is the entity class. As the astute player might have
realized the program would seemingly have three different elements: Namely
the walls, the player and finally the monsters. Yet I have only two
classes. The reason for this is that the monsters and the player are in
fact the same object. The only differences between them is, that their
appearance and their behavior. This is a deliberate choice from my end.

The entities in my game are all define the same way, by their position,
their dimensions and their appearance. These aspects don't change from
entity to entity.

All entities also share many methods of behavior. They're all incapable of
moving through the walls of the maze and they're all able to circle around
from one edge of the screen to the other.

So in my program all entities are at their core the same, but their are
differences that gives an entity the title of 'Hero' or 'Monster'.
Firstly there is only one 'hero'. The 'hero' in the programming the 'hero'
is only able to run. But cannot do anything other than that. (To an extend
I will return to this point later).

On the other hand are the 'monsters'. As opposed to the 'hero' multiple
'monsters' can exist at once. The behaviors that define a 'monster' can
chase, roam and kill. Here we see that a 'monster' are allowed more
freedom than the 'hero' if you call it that.

![](Gif_miniX6_2.gif)

What I find interesting is, that despite the written behaviors that are
coded into the game. There are another factor that makes a difference
the different entities. The fact that the 'hero' is controlled by a player
is a huge difference between the 'monsters' and 'hero'. This is because
the 'hero' through the human player is able to see. Not just its immediate
surroundings, but the entire maze. As opposed to this the 'monsters'
cannot see whatsoever. The only thin they know is their own location and
the location of the 'hero'. This ca be seen through their interactions
with the border. The 'hero' is able to see the big picture, while the
monsters cannot. This makes it possible to trick the monsters into running
across the entire map if the player escapes through the border.

![](Gif_miniX6_3.gif)
### Why only one class?
My reasoning for only making one class to fit both monsters and the hero
is simple. To me many classical understandings of good and evil is based
only on appearance and certain behaviors. In war the enemy are the things
that do not look like us. Had I edited some of the methods in the game by
removing the monsters ability to hurt the player in any way or giving the
player the ability to attack the monsters, then the in game world and context
would have been radically changed. By removing the monsters ability to kill.
I don't think the players initial reaction to the monsters would change. The
player would still try to evade the monsters, because those are the conventions
set up in a lot of games. "That which is not us is an enemy."

On the other hand by giving the player the ability to kill. The world would also
change since the only difference in this case would be that one type of entity
would be forced to repeatedly approach the vastly more intelligent monster
running around causing massive destruction.
### The human and the computer
The human computer relationship in this case is really interesting.
When trying to balance the game I found that even though the AI I have made
for the computer to follow is really rudimentary at best, it would still
outperform me at times through its sheer efficiency at controlling the monsters
therefore I had to slow them down as they quickly would overwhelm the player.
Even now a bad maze can be detrimental, as the computer can be efficient at
closing the distance which takes newer player by surprise.
