class Wall {
  //Sets up parameters that can be filled with argumetns.
  constructor(_x, _y, _w,) {
    this.x = _x;
    this.y = _y;
    this.w = _w;
    this.h = 20;

  }
  show() {
    //Makes a rectagle based on the input from the constructor.
    fill(80);
    rect(this.x, this.y, this.w, this.h);

  }

}
