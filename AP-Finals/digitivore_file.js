class File {
  constructor(_x,_y,_p,_a,_n) { //Sets up the class atributes. What makes a file a file.
    this.x = _x;
    this.y = _y;
    this.pod = _p; // pod = process of decay
    this.age = _a;
    this.name = _n
  }
  show(version) {
    image(fileImg[version],this.x,this.y,60,60); //this displays the picture of the file. The version changes based on the argument that is added by the RUNME. This is tied to the integer returned by the decay atribute.
    textWrap(CHAR); // tells the text that it should split when reaching edge of "box"
    textAlign(CENTER,TOP); //The textbox is drawn from the center of its x-axis and the top of its y-axis.
    text(this.name,this.x-15,this.y+60,95,40); // "95,40" is the invisible "box" that stops the text from continueing (interrupts the file name).

  }
  decaying(count) {
    if(count%3 == 0) { //Classic clock based on the arguments added in the RUNME. If the frameCount is divisible in 3 the statement is true.
      let decay = random(1); //here a random number is generated from 0 - 1
      if(decay < 0.5) { // 50% chance of decaying, technicly it is not 50 / 50
        this.pod = this.pod
      } else if(decay >= 0.5 && this.pod < 21) {  // If the decay value is bigger or equal to 0.5 and the this.pod is less than 21 (not dead yet), then this.pod is plussed with itself.
                                                  // We need to use greater than equal because we need to take into account any possible result of the random number generation.
        this.pod++; //
      }
    }
      return floor(this.pod); //This returns the pod as an integer. We round down because we used to increment by decimal values, this was not compatible with our intent to use the return as an index value outside of the function.
  }
  dead() {
    if(this.pod == 21) { // If this.pod reaches the number 21, it dies, if not, it isn't
      return true;
    } else {
      return false;
    }
  }
 //I remember having to make the dead function I cannot remember why at the moment. But I think it wasn't enough to use the if pod == 21.
  aging(count) {
    if(count%4 == 0) {  //hvis count går op i 4, så + age med én
      this.age++
    }
    if(this.age > 15) { //When age is greater than 15 the function return true (begins decaying) otherwise it returns falls.
      return true;
    }else {
      return false;
    }
  }
  interact(otherX,otherY) { //These arguments pass information to the functiion. De blir erstatet af mouse x og mouse y når den er i brug.
    if(otherX > this.x && otherX < this.x + 60) {   // if the mouse is in these coordinates, and the mouse button is pressed, this.age and this.pod returns to 0
      if(otherY > this.y && otherY < this.y + 60) { // The +60 is making the hitbox where you can interact with the file. The X and Y are the coordinates.
        if(mouseIsPressed == true) { //This is an interesting choice we used mouseIsPressed the result of this is that the mouse can be held down and the files will be reset.
          this.age = 0;
          this.pod = 0;
          //We tried to add a change in mouse icon, but we did not have the time. there were troubles with changing it back once it had been activated.
        }
      }
    }
  }
}
