//Variables for the program
let fileImg = []; let groundImg; // file img: array hvor billed-filerne sættes ind. groundImg: vinduerammen.
let fileName; let newName; let newfileName; //Variable, der bliver brugt til at lave navne basseret på json filen.
let files = []; //Array til hvert fil objekt bliver puttet ind.
let curX = 0; curY = 0; let fileX = 0; fileY = 0; // fileX is bound curX osv. used for windowresizing.
let curDecay; let curAge; //curDecay = how decayed it is. curAge = time lived/current age.
let count; //Is used with every clock

let growth; // Variable that changes the speed of upload.

function preload() {
  for(let i = 0; i < 22; i++) { // load all animation-images.
    fileImg[i] = loadImage('Assets/file_img'+ i +'.png');
  }
  groundImg = loadImage('Assets/digitivore_Window.png'); // Load and define the UI image
  fileName = loadJSON('fileName.json'); // Load and define the Json file
}

function setup() {
  createCanvas(windowWidth, windowHeight);//Make canvas
  frameRate(5);//With this we have the power
  curDecay = 0; curAge = 0;
}

function draw() {
  background(250, 255, 209); //Make background
  image(groundImg,0,0,windowWidth-15,windowHeight); // window-png
  //Just calls the custom made functions. They will be explained in detail.
  generateName();
  matrix();
  file_upload();
  file_logic();
}

function generateName() {
  newName = fileName.nameDirectory; // Brug vores Json som hedder fileName. Inde i den fil skal den loaded "nameDirectory", som er et step inde i Json filen.
  newfileName = newName[int(random(newName.length))].name; //finds random json-value. int = helt tal, pga. array ikke har kommatal. "name:" i json-filen bliver til .name herinde - det er kategorien.
}
function matrix() {
  /*
  By using the modifer variable we are able to change the distance between
  files on the fly. So that we can take into account multiple different
  windowsizes. We use this method instead of the translate syntax
  because it did not register the mouse properly. Since mouseX and mouseY are calculated from a global origin point.
  */
  let modifier = (windowWidth - (windowWidth/5))/6; // a 5th of the screen is subtracted to start the file on the edge of window-png (which is now the new point 0). then, the remaining screen is divided in 6 (for the file-objects' placements)
  //Both fileX and fileY are
  fileX = curX * modifier + (windowWidth/5); // modifier is being mutiplied with our curX, this is then plussed with our inner square. This is done by removing the 1/5 of our screen, cause of our UI. moves placement from edge of window-png to NEXT to it.
  fileY = curY * (modifier/1.5) + (windowHeight/10); // same thing, just on the y-axis.
}

function file_upload() {
  growth = (floor(1+files.length/10)); // floor runder ned. 10 is the amount of rows. 1 is plussed with the amount of objects on the screen, this is then divided by 10. resulting in dropping its speed after creating a lot of objects. because framerate is 5, it would take a long time if now divided by 10.
  count = frameCount; // Count is equal to the framecount
  /*
  Biggest edit: we need to do it with push, since once
  we start to splice the program gets confused, if we use the old method.
  (old method = variable was index++, which would add to the existing number of file-objects in rows of 6. Because our files splice and disappear, it would not know what to put in the new gap that had been made. PUSH rykker filerne på plads!)
  This is because the old method adds the files by index whereas push
  just adds to the end. The other one would go file[0],file[1],file[2] and even
  if file[2] was removed it would still go to file[3].

  Here we also get the importance of using the framecount to make the clocks
  we talked about how we could have used the build in clock to create the
  timer, but by doing it with frame count we can control the passage of time
  inside the world we have made
  */
  if(count%growth == 0 && curY < 10) { //curY < 10 to stop the file-generation (so it doesn't continue offscreen). % = modulo, which means that if divided and the remainder is 0, it is "true." True results in a new file being generated
  files.push(new File(fileX,fileY,curDecay,curAge,newfileName));
  curX++;
  }
  if(curX > 5) { //When curX is larger than 5, then curX is set to 0 and our curY is plussed with one (resulting in a new row).
    curX = 0;
    curY++; //when the current x postion is larger than 5 which is postion 6 it will add a new row
  }
}
/*
It is actually a pretty interesting process. We use the
decaying funtion in the File class and get it to return its current
value. This value is then used as an index value in the show function.
So that a file will change what picture it shows every time the pod increases.
*/
function file_logic() {
  for(let i = 0; i < files.length; i++) { //A for loop with the condition being the amount of objects.
    files[i].interact(mouseX,mouseY) //This function controls the interaction with the mouse
    files[i].aging(count); //Has a parameter based on the count. this is because aging is another clock that is present within the class.
      for(let j = 0; j < 5; j++) { //Essentially this loop just speeds up the decay process - it is leftover from another version
        if(files[i].aging() == true) { //Initiates the decomposision process when aging returns true. Which is does when it is larger than 15.
        files[i].decaying(count); //Here is another clock based on the argument count.
        files[i].show(files[i].decaying()); //This part is interesting as we use the return of one atribute of the class as an argument for another. This argument decides which of the images the file should show.
      } else { //If the decay process is not initiated the file will just show its unaltered picture.
        files[i].show(0);
        }
      }
    }
  for(let i = 0; i < files.length; i++) { //There REALLY is NO reason for this loop to be separate, I think this is just for structure.
    if(files[i].dead() == true) { //This is the abillity of the class that checks if the file has decomposed.I initially wanted to use decaying for this purpose but I could not get it to return a boolean and an integer at the same time.
      files.splice(i,1); //This removes one objekt from the array. It is somewhat important to write i instead of files[i], as it seems there is a difference between those two. If files[i] was written, it would splices
    }
  }
}

//Wander not cross this border only the twisted minds of the dammed exists beyond
function grid() {
  let modifier = (windowWidth - 0)/5;
  let rows = 10;


  for(let i = 0; i < rows; i++) {
    fileY = i * modifier;
    placement[i] = [];
    for(let j = 0; j < 5; j++) {
      fileX = j * modifier;
      placement[i][j] = [fileX,fileY];
    }
  }
  return(placement)
}
//placement[curY][curX][0],placement[curY][curX][1] //Det her er coordinaterne der er defineret i arrayet.
