## miniX2 Hand in
Link to Code: https://gitlab.com/Vinmek/aesthetic-programming/-/blob/main/miniX02/miniX2.js
Link to Program: https://Vinmek.gitlab.io/aesthetic-programming/miniX02/indexX2

### What does the program do?
The program I have created asks the user, how many eyes the user
has. Based on this information it then generates a rudimentary
emoji with a mouth and either two eyes or an eye and an eyepatch.

![](miniX2_1.png)

The program is able to generate three different mouth types. Each
being able to vary in size. Likewise the program can generate two
different types of eyes that is also able to change size.

Do to time constraints as well as the growing complexity of the
project I was forced to cut certain features from the program.
My original plan was to give the user the ability to choose what
side their remaining eye is placed on, but that complicated the
creation of buttons so I deleted it. I also had plans on making
the eyes placement independent of one another, but chose to cut it
since it was not strictly necessary for the program.

I did have plans for more eye types, and for the eyes to generate
as separate types instead of the paired version that ended up in
the final version.

The way the program functions is through generating a large amount
of random variables, that is used to pick the eye and mouth types,
as well as placement and in some areas also the size of the
respective feature. I have written a pretty thorough description of
the process in the notes of the program itself.
The result of this process is a bunch of varying faces, as can be
seen in the pictures below.

![](miniX2_2.png)

![](miniX2_3.png)

![](miniX2_4.png)

![](miniX2_6.png)

![](miniX2_7.png)

As stated in the notes of the program I have taken inspiration for
the look of my program, from Multi by David Reinfurt, as it
seemed to solve my own problems with the issue of inclusivity in
emojis. It should be stated that I have not seen and code from the project
and because of that I have to my knowledge not copied it deliberately.

### My Concept
So when it comes to the idea an plan behind my work. Is a problem I found when
thinking of how to make emojis more inclusive. I found myself being frustrated
because it seems that no matter what, someone will end up being excluded.
I don't believe that a lack of inclusion is the problem, I rather believe that
a concept such as universal inclusion is fallible in the first place.

I don't believe it to be correct that we expect accuracy when we try to boil
down human beings into a tiny image. Because no image can precisely recreate
a human being. The same point can be interpreted from the Excavating AI project
by Kate Crawford and Trevor Paglen. This project put a spotlight on how training
sets used in machine learning of picture recognition programs were inherently
racist and biased. The argument here was that the reason for this is that bias
is so ingrained in every step of the process from what pictures to include to
the people actually labelling the pictures that the idea of making an unbiased
training set would be almost if not totally impossible. This to me shows, that
not even humans can know a person from just a picture. Because humans are so
complex.

This is the reason I choose to distance myself from more inclusion, and instead
tried, and in some ways failed to make a simple emoji, that did not match
anything about a human other than their eyes and mouth.
I say failed because I immediately hit fork in the road. As soon as I made a
choice to include eyes, I had already excluded some people. And I still do.
I chose to make the head square and the background gray, to avoid the emoji
being too close to something natural. But the result of this is that people
struggle to realize it is a face. And when it comes to the number of eyes.
I have chosen to limit the choices between one and two eye. Ignoring the
possibility of someone having no eyes or people having both eyes, but one being
blind. I ended up not adding a no eyed version because I wondered how many blind
people even use emojis. An interesting thought in its own right.

Sadly I ended up realizing the fact that I would end up excluding people the
moment I started making any sort of decision.
