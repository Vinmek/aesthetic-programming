/*
I want to start with an apology. I am so sorry to whoever is going to try
to go through this. I will do my best to explain every detail. As well as the
ways stuff fits together. As I have tried to do with limiting the mess you are
about to witness

This program starts out with af bunch of variables. This is because a lot of
the program is generated ramdomly. I do not have a better way of doing this as
of yet.

It is important to note, that the programs functions can be separated into
three distinct groups. Based on what they do. the variables reflect this,
as e variables are affecting the eyes, and m variables the mouth. The remainign
variables are affecting the interface.

Finally It should be stated that I took inspiration from Multi by David Reinfurt
for the setup of the emoji. Since it matched well with my ideas on the subject
I have not to my knowledge copied any code though.
*/
let i;
let tx = "How many eyes do you have?"
let tx1 = ["One Eye", "Two Eyes"];

let e;
let ex;
let ey;
let ed;
let ev;

let m;
let mx;
let my;
let md;
let mr;
let ml;

/*
It is important when going through the code. That I am using custom functions.
THis means that I write the actual code, not in the setup or draw functions,
but somewhere else. And then call them into those functions.

The real functions are placed below. I would advice to not open up too many at
once as that can be really confusing really quick.
*/
function setup() {
  // The standard start. Creating a canvas.
  createCanvas(600,700);
  // Here I'm assigning values to the variables I have made.
  i = 0;
  // The e and m variables can be found in their respective functions below.
  e_Random();
  m_Random();
}

function draw() {
  //I think we all know this one.
  background(100)
  /*
  Okay this is the forst conditional statement. This dictates what is shown.
  It is affected by the buttons. I had a hard time getting them to work.
  Originally I wanted more choices, but I could not make it work.

  These options are all controlled by the I variable from earlier.
  They are pretty similar syntax-wise, since they are all just calling
  other functions.

  There is one interesting point. When assigning the i values
  for the if statements I have chosen 2 and 3 this is a relic from an
  earlier version with multiple questions. I had to esure that pressing
  a button twice resulting in the same amount as pressing another once.
  */
  if (i == 0){
    start();
    press();
  }
  if (i == 2) {
  //One Eye
  head();
  eye();
  patch(215);
  mouth();
}  else if (i == 3) {
  // Two Eyes
  head();
  eye();
  eye(215);
  mouth();
  }
}

function e_Random() {
  // As stated above almost everything is random.
  /*
  Important to note I convert some of these into integers, as
  I use them for conditional statements.
  */
  e = int(random(1,3));
  ex = random(120,260);
  ey = random(110,250);
  ed = random(10,50);
  ev = random(0,15);
}
function m_Random() {
  // As stated above almost everything is random.
  //The mouth contains more values, as I made some more complicated shapes.
  /*
  Important to note I convert some of these into integers, as
  I use them for conditional statements.
  */
  m = int(random(1,4));
  mx = random(225,375);
  my = random(450,550);
  md = random(5,60);
  mr = int(random(3,7));
  ml = 30;
  print(m);
}
function start() {
  // This part isn't really that interesting.
  // Displays text. and sets the size of said text.
  textSize(30);
  text(tx,100,150);
  /*
  Displays the "buttons". I'm sure there're better ways of doing this,
  but I could not make it work.
  */
  rect(75,200,200,100);
  rect(325,200,200,100);
  //More text.
  textSize(40);
  text(tx1[0],96,260);
  textSize(40);
  text(tx1[1],338,260);
}
function press() {
  /*
  This is a pretty interesting part. Especially since it was a nightmare to
  get working. I have had more trouble with this one part, than the rest of
  the program.

  This part checks whether or not, the mouse has been pressed, and wether or
  not the mouse is over the area of the button. It is a lot of math. That I
  would need a whiteboard to explain, so I will skip past that.

  This part also includes a second if statement. This is to ensure that
  the increment only happens once. One might argue, that I could put it in
  setup instead of function, but that breaks the buttons. I asume it is because
  the program cannot update the mouseX and mouseY if this is the case. It could
  be something to look into.
  */
  if(mouseIsPressed) {
    if (75 < mouseX && mouseX < 275 && 200 < mouseY && mouseY < 300){
      if(i != 2) {
        i = i + 2;
      }
    }else if (325 < mouseX && mouseX < 525 && 200 < mouseY && mouseY < 300) {
      if(i != 3) {
        i = i + 3;
      }
    }
  }
}
function head() {
  /*
  This is relativly simple. Compared to what is coming up. This loop draws the
  same line twice to make the boundary of the face. it does so by changing the
  x coordinate dependent on what the n variable is.
  */
  for(let n = 0; n <= 1 ; n++) {
    strokeWeight(4);
    line(60 + (n * 480), 75, 60 + (n * 480), 600);
  }
}
function eye(x_mod = 0) {
  /*
  Okay now we begin to get to the more interesting. In setup I genearate a
  random variable integer. That is either 1 or 2. When the program draws this
  function the variable dictates the type of eyes shown.

  I had planned for more eyes. And for more itterations of the current eyes.
  I wanted to rotate the crosses randomly, as well as make a version of the eye
  with eyelids. But in order to keep the program "simple" I have scrapped those
  functions.

  I also had plans on letting the user pick what eye they had missing, but
  it would have been tricky to do. Though thinking back I might know of a
  way to do it. Making a third button and so forth.

  I also planned to make a variable that would make a little bit of variance
  between the eyes positions. But I scraped the idea. Because this is alredy a
  pretty complex thing.

  It should be notet that I use a parameters in my function. These are used to
  costumise the function when I call it. The x_mod is to modify the x coordinate
  so I minimize the amount of variables I need to make and keep track of.
  The reason for the = 0 part is to make it a standard value, which means
  x_mod is 0 unless I state otherwise.
  */
  if(e == 1) {
    // Simple ellipse
    noFill()
    ellipse(ex + x_mod, ey, ed)
    strokeWeight(4);
  } else if (e == 2) {
    /*
    Again there is math, I used the same x and y as
    with the circles and modifed them.
    */
    strokeWeight(6);
    line(ex - ed + x_mod, ey, ex + ed + x_mod, ey);
    line(ex + x_mod, ey - ed, ex + x_mod, ey + ed);
  }

}
function patch(x_mod = 0) {
  /*
  Just a fun little thing to balance out the face, when you only have one eye.
  It follows where the eye would have been normally.
  */
  fill(0);
  ellipse(ex + x_mod, ey, 100);
  strokeWeight(4);
  line(ex + x_mod, ey, 225, 75);
  line(ex + x_mod, ey, 540, 325);
}
function mouth() {
  /*
  This is the complicated part of the program. It works similarly to the eyes.
  and the two first itterations of the mouth is also based on how the eyes are
  drawn.

  It is the third version that is tricky. Again there is a lot of math. That is
  dificult to explain without drawing it. This ode draws the "stiched" mouth.
  It does this through a for loop. Like with the head. This loop dictates the
  placement of certain segments of the mouth. These segments are made from
  one horizontal and one vertical line.The number of segments in a mouth is
  random. Between 3 and 6 one of the other areas where the number needs to be an
  integer.
  The only thing that is not random is ml. ml decides how long the horizontal
  line each mouth segment is.
  */
 if(m == 1) {
   ellipse(mx,my,ed - 10);
   strokeWeight(4);
   if(md > 30) {
     fill(0);
   } else {
     noFill();
   }
 } else if (m == 2) {
   line(mx - md, my, mx + md, my);
   strokeWeight(12);
 } else if (m == 3) {
   strokeWeight(6);
   line(mx - md, my + 10, mx - md, my - 10);
   for(let v = 0; v < mr ; v++) {
     strokeWeight(6);
     line(mx - md + (v * ml), my, mx + ml + (v * ml) - md, my);
     strokeWeight(6);
     line(mx - md + ml + (v * ml), my + 10, mx - md + ml + (v * ml), my - 10);
  }
 }
}

//This is a graveyard of old code. Only the Fool enters here.
/*class bton {
  constructor(_x, _y, _l, _w) {
    this.x = _x;
    this.y = _y;
    this.l = _l;
    this.w = _w;
  }
  show() {
    strokeWeight(2);
    rect(this.x, this.y, this.l, this.w);
  }
  interact() {
    d = dist(mouseX, mouseY, this.x, this.y);
  }
}*/
/*console.log(tt);
if (i <= 1) {
  if (tt === true) {
    button1 = createButton(tx[i]);
    button2 = createButton(tx[i+2]);
    button1.size(200, 100);
    button2.size(200, 100);

    button1.position(sur1, 200);
    button2.position(sur2, 200);
    button1.mousePressed(b1);
    button2.mousePressed(b2);*/
