class File {
  constructor(_x,_y,_a) {
    this.x = _x;
    this.y = _y;
    this.age = _a;
    this.old = 0;
  }
  show(version) {
    image(fileImg[version],this.x,this.y,60,60);
  }
  decaying(count) {
    if(count%3 == 0) {
      let decay = random(1);
      if(decay < 0.5) {
        this.age = this.age
      } else if(decay >= 0.5) {
        this.age++;
      }
    }
    if(this.age == 5) {
      this.age = 0;
    }
    return floor(this.age);
  }
  aging(count) {
    if(count%10 == 0) {
      this.old++
    }
    if(this.old > 0) {
      return true;
    }else {
      return false;
    }
  }
  interact() {

  }
}
