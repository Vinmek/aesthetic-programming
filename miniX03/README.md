## MiniX3 Time stopper
My Code - https://gitlab.com/Vinmek/aesthetic-programming/-/blob/main/miniX03/miniX3.js

My RUNME - https://vinmek.gitlab.io/aesthetic-programming/miniX03/indexX3
### What is time?
Time to me seems to be anywhere and seems to be both an immaterial and material
thing. I call it on one hand material due to the perception that it can be wasted
sayings such as "time is money" and the practices of rewarding people for giving
their time also infer that time does have a value of sorts. But despite that
time remains immaterial, you cannot touch time, even though time seemingly is
able to touch anything and everything. And time seems to be not constant, but
ever fluctuating. Because of this property of time, that it is not constant,
but vary based on situation by which I mean that two people in similar places
can experience time in vastly different ways.

To further this point I will use three examples:

The first example of how time is not constant comes from an experience told
to me by my grandfather.
When working as a teacher. He would experience how the same two hours during
written exams would be experienced as very different stretches of time by
the stressed students and the bored superintendents set to watch out for cheaters.

The second more scientific example is how to stop watches calibrated with one
another will dilate, if one is flown around at high speeds.
And the last is the way that the measuring of time can affect the perception of
it. As stated in the text: "The ends of time". Measuring time with a sundial
will make hours seem longer in areas at latitudes further from the equator.
As well as having time "stop" at sundown.

This property of time, that it is not a constant we can always depend on many
different factors, makes it tricky to decide how to standardize time, if such a
concept is at all a possibility. It is difficult on earth today, but imagine
having to standardize time across multiple planets and maybe even star systems.

### My miniX
#### Stopping time
As I stated above the sundial gives the impression that time stops, when the sun
goes down and starts again at dawn. But that is an illusion, time continues to
move forward. And the fact that time does that, might be the only constant thing
about it. When not taking into consideration black holes, where the concept of
time might break down entirely. A concept I will only mention and not go into
further detail about in this read me. I wanted to tackle the idea of stopping time
with my program or more closely the futility of that idea.
#### My Sketch
My sketch is works best, when in motion, but I will try to explain it here.
In my sketch I have created a clock, that keeps going forward as a symbol of how
time keeps moving. The clock is made in for parts the shell is just an ellipse
while the hour indicators are made using two for loops that draws lines of
differing lengths while rotating the canvas. the last part are the two clock
hands that is rotated by increasing the angle of the rotation.

Over the clock is displayed the question "Stop time?", hopefully prompting the
user to try and stop time.
![](Billede_miniX3_1.png)

This action can be attempted by moving the mouse inside the clock and keeping it
there which stalls the clock. This will make the clock vibrate more and more
violently until the time starts once again now more uncontrolled than before
the text at the top changes now displaying "You cannot". The clock now ignoring
the subjects attempts for further manipulation.
![](Billede_miniX3_2.png)
#### Reasoning
My reasoning behind this sketch comes from my experiences with trying to make time
stop. Mostly as a precursor for uncomfortable events such as exams.
In my experience you can attempt to make time stop, and it might even feel like
you are doing it for a while, but when you realize that you in fact did not stop
time, time suddenly seems to move much faster than before.

My hope with the program was to capture this futility in some way. And the way I
went about it ended up presenting an interesting effect of my code. The way the
sketch is programmed makes it seem like time has stopped, when in fact nothing
in the program really changes. The hour hands stopping is an illusion singe I
still make use of a self made internal clock to count a certain timeframe, before
the program restarts. I found this interesting since it was part of my original
intention, but still supports my idea of the illusion of stopping time.
