/*
Again this program is a mess so I am sorry before hand.
I have tried to write down a walkthrough for it.
A lot of this is just wierd math equations, because that is apparently how
my brain works.
*/
// An assortment of different variables
let st_w;
let st_h;
let hour_mark;
let hand_speed;
let hand_mod;
let dist_time;
let count;
let rumble;
let flag;

function setup() {
  // In setup I have mainly assigned values to my variables.
  // Important for the program is that it is dependent on the window size.
  st_w = windowWidth;
  st_h = windowHeight;
  hour_mark = 30;
  hand_speed = 0;
  hand_mod = 0;
  flag = 0; //Note the term flag is derived from the videogame term 'eventflag'
  createCanvas(st_w, st_h);
}
function draw() {
  // Here I have translated the origin for the entire program.
  /*
  When looking for something most of the functions are placed in the order
  they appear in the draw function, besides mouseInteract that is placed at
  the bottom. Since it is placed alongside the time_stop function.
  Placementvise its important that mouseInteract is placed before the watch
  because if this is not the case. The mouse interact becomes unable to
  effect the clock. In addition the display function is required to come
  before the mouseInteract because it othervise would also be vibrating.
  */
  background(0);
  translate(st_w/2, (st_h/2)+20)
  display();
  mouseInteract()
  shell();
  hour_indicators();
  watch_hand1();
  watch_hand2();
}

function display() {
  push();
  translate(0,-(st_h/4) - 50); //Translate so that the text is ofset from center.
  textAlign(CENTER,CENTER); //I make it so text posision is drawn from the center.
  //I use the flag to decide what text is currently displayed on the screen.
  //The flag is manipulated in the time_stop function.
  if(flag == 0) {
    textSize(40);
    fill(255);
    text("Stop Time?", 4, 0);
  } else if(flag == 1) {
    textSize(40);
    fill(255,0,20);
    text("You Cannot", 3, 0);
  }
  pop();
}
function shell() {
  // A simple case of making two ellipses nothing crazy here.
  fill(255);
  ellipse(0, 0, 8);
  push()
  noFill();
  stroke(255);
  strokeWeight(5);
  ellipse(0, 0, st_h/2); //The ellipses diameter is dependent on the window.
  pop();
}
function hour_indicators() {
  //A little more complicated I used two different loops to draw hour indicators.
  //I increase the position with the control variable in the forloop.
  for(let in1 = 0; in1 < 4; in1++) {
    push();
    angleMode(DEGREES);
    rotate(in1*90);
    stroke(255);
    strokeWeight(4);
    // This is maybe the trickiest part. There is a lot of math.
    // I draw the line from the stroke of the shell-ellipse.
    line(0, -st_h/4, 0, (-st_h/4) + hour_mark);
    pop();
  }
  for(let in2 = 0; in2 < 4; in2++) {
    angleMode(DEGREES);
    rotate(in2*90);
    for(let in3 = 0; in3 < 2; in3++) {
      push();
      angleMode(DEGREES);
      rotate(30 + (in3*30));
      stroke(255);
      strokeWeight(4);
      //It is the same down here just that i do not have the same length of maek.
      line(0, -st_h/4, 0, (-st_h/4) + (hour_mark/2));
      pop();
    }
  }
}

/*
The watch hands morks the same way. They are rotated by rotating the canvas.
I get a differing speed by dividing the speed with 12.
The idea of the speed_mod is to increase the speed once the flag has changed.
*/
function watch_hand1() {
  push();
  rotate(hand_speed);
  stroke(255);
  strokeWeight(4);
  line(0, 0, 0, st_h/4.5);
  pop();
}
function watch_hand2() {
  push();
  rotate(hand_speed/(12-hand_mod));
  stroke(255);
  strokeWeight(4);
  line(0, 0, 0, st_h/8);
  pop();
}

function mouseInteract() {
  //This function calculates the distance between the mouse and the ellipsecenter.
  //It also resets the count when the mouse is not over the clock.
  //And is blocks the interaction when the flag is active.
  dist_time = dist(mouseX,mouseY,st_w/2, st_h/2);
  if(dist_time <= st_h/4 && flag == 0) {
    hand_speed += 0;
    time_stop();
  } else if(flag == 1) {
    hand_speed+=10;
    hand_mod = 10;//Here I increase the second hour hands speed by a little.
    } else {
      hand_speed++;
      count = 0;
      }
}
function time_stop() {
  //Here I use the variable rumble, that is tied to the count variable.
  rumble = 1.5 + (count/50);
  translate(random(-rumble,rumble),random(-rumble,rumble));
  count++ //Count is being increased as long as the mouse is over the clock.
  console.log(count);
  if(count >= 200) {
    flag = 1;
    // Here flag is changed. I went with flags instead of a boolean
    // To have open the possibility to add more events if necessary.
  }
}
