let n = 0.0;
//I assign a variable
function setup(){
createCanvas(400,400);
background(100);
frameRate(30);
/*
sets the framrate to 30 fps(frames per second).
It is to make the circle move more smoothly
while not having the colors go crazy.
*/
}

function draw(){
/*
The idea here is that when the b key is held
the program will draw a background
*/
if (keyIsDown(66)) {
background(100);
}
  n = n + 0.01;
  let x = noise(n) * width;
  let y = noise(0, n) * width;
  ellipse(x,y,50,50);
  /*
  This is my favorite part of the code
  the gist of it is, that I change n by adding 0.01
  to it.
  Because of this the variable keeps changing
  I then add it to a noise syntax that picks a random amount to move
  the reason I multiply with the width is to set the borders
  if not for this the ellipse would go of the canvas
  */
  fill(random(200),random(200),random(200));
  //fills the drawn ellipses with af randomly generated color
}

function mousePressed(){
  noLoop();
  //stops the loop when mouse is pressed
}
function mouseReleased(){
  loop();
  //restarts loop when mouse is released
}
