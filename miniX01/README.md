## Hand in for miniX1
Link to code:  https://gitlab.com/Vinmek/aesthetic-programming/-/blob/main/miniX01/miniX1.js

Link to program: https://vinmek.gitlab.io/aesthetic-programming/miniX01/indexX1
### Coding as opposed to traditional writing
It is rather interesting to code. Unlike with writing
"normally" writing code needs to be way more precise.
In traditional writing you are able to handwave certain things.

An example could be getting from one point to another.
From my experiences with traditional writing
I know that a human can fill in gaps in the story.
An author does this, through short hand,
and by invoking past works and conventions.

If this is possible in coding, which I believe it is,
I myself am not a proficient enough coder to do it.
I have experience with writing and some experience reading texts
in an analytical way.

The main difference I find between a human and a computer is
that when writing code to a computer the "digital author"
i.e. the coder is barred from relying on author shorthand.
Shorthand seems to be present,
but it works on a different set of rules.
A computer needs to be given exact instructions and unlike the
human brain that is able to autocorrect
(something that makes it very hard to catch spelling
errors I might add), a computer cannot make the human leaps
in logic to better understand.

All that being said, then coding as a language is able to be far
more precise. This is also what I mean when I state that
I believe there to be some form of "digital" shorthand.
When writing code a lot of things seems to be going on behind
the scenes. Stuff that I do not know what is.
But the crux of the matter is. That by only writing maybe 10
words a lot of things have happened and a circle is drawn.
It is fascinating, yet daunting to imagine what setting up the
conventions and quote on quote clichés of the programming
language which makes it possible for the computer to draw meaning
from a single word.

### My experiences while coding
This last part nicely sets the basis for my experience with coding for
the first time. To me it was a strange mixture of awe and mundanity,
of struggles of success.

There is a strange feeling of power, when you like some sort of god
with a single word can bring something into existence, even if that
"something" is nothing more than a circle or square. As stated before
imagining what is going on within the computer is also somewhat
mind blowing. On the other hand because you need to be so precise
with what you write and the many details you need to account for.
The whole experience is dragged back down to the more mundane world.

I also found myself going through interchanging stages of successes as well
frustrating failures where I just could not figure out what was refusing
to work. I am not totally new to programming. I have not written anything from
scrap before now. I have at least tried editing in code before, as well as
doing programming in the style of legos. The result of this is that I do
have a vague knowledge of what should be possible. This vague notion oftentimes
left me with the knowledge of what I wanted to do, as well as the knowledge
of what I should be able to do, but with no idea of how I would actually
do the thing I wanted.

### My miniX1
Link to code: https://gitlab.com/Vinmek/aesthetic-programming/-/blob/main/miniX1/miniX1.js

I ended up trying to take the drawing of circles to a new level.
I quickly learned that it was possible to make the computer generate
a random set of coordinates for the drawn circles. From this discovery I found
that the color of the drawn circles could be randomized as well, and I learned
to manipulate the framerate of the draw function, which made it possible to
control the speed with which new circles would appear.

![](miniX1_Billede2.png)

My next development was the discovery of the importance of syntax placement
I found that by adding a syntax that would color in the background I would be
able make it look as if the dot was only jumping around rather than continuously
drawing new circles on top of each other.

The function is actually still in the final program and can be activated by
holding the b key on the keyboard. Another feature also exists to stop the
process which is activated by pressing the left mouse button.

The final development in the program was when I added the noise syntax.
This was the most annoying to figure out, but in the end it really improved the
program in my opinion. I wanted to make the circles appear in a random, but not
completely random way. I wanted them to appear based on where the previous
circle appeared. In doing so I have managed to create a rather captivating
screensaver.

![](miniX1_Billede.png)

The full functions of the program is best showed when running the program.
The program can be accessed through this link, and I have written a short
description of the possible interactions:
https://vinmek.gitlab.io/aesthetic-programming/miniX1/indexX1

- Press and hold b
  - Draws a background as well as semi resets program.
- Press left mouse button
  - Pauses the program until the button is released.

### What programming is to me
So as I have mentioned throughout this text I have quite a confused relationship
to towards code and programming. It seems so vast and complicated, but when
I sit down with it, it does make sense. It does not feel as if it would be
impossible to create something complex, but more as if I just need to put in the
time to write the code.

This feeling makes me wonder, why I have not started coding before now. Whether
I have needed a starting point to jump off from or if I have lacked the willpower
to make the jump in the first place, I cannot say. But I do find it puzzling
that I would be 22 before trying to code properly for the first time.

After programming I have found myself being more conscious about the amount of
code that is existing around me. Seeing the amount of code I need to make a circle
move around on a screen, I cannot begin to imagine how much is needed to make the
trams in Aarhus connect with each other.

It is thoughts like those that make me extremely excited to improve my own
programming.
