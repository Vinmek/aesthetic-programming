/*
This is a pretty convoluted program, but I have tried to separate it out in
order to make it easy to navigate.
*/
//These first variables are connected to the entities in the game.
let hero; let heroImg; let monImg;
let startx = 300; let starty = 55;
let r = 8;

let walls = []; let monsters = []; //These are arrays for the objects I use.
let fire = [];

//These variables are working the respective widths, and initial positions of the walls.
let wWidth = [];
let wXPos; let wYPos;

//These form the timer, that generates new skeletons in the maze.
let time = 0; let frame; let minMon;

//These are the checks that hinder the entities from moving through the walls.
let topSide; let bottomSide; let rightSide; let leftSide;
let topMon; let bottomMon; let rightMon; let leftMon;

let facing; let killer = false; //The state of the player

//I still have a lot of functions and now an additional two Js files.
//Hang on this is going to get wild.
function preload() {
  //We load in the sprites for the program.
  //The designs are NOT made by me, due to time issues.
  heroImg = [loadImage('assets/Sprite_Hero_R.png'),loadImage('assets/Sprite_Hero_L.png')];
  monImg = loadImage('assets/Sprite_Skeleton.png');
}
function setup(){
  createCanvas(600,600); //Creates canvas I intentionally limit the playing field.
  //I run a couple of setup functions that makes the maze and obsticles.
  //I will explain in further detail in the respective functions.
  booleanSetup(); //Sets up for collsion detection.
  wallSetup(); //Walls
  hero = new Entity(startx,starty,r); //The player
  //frameRate(10); //Testing if everything is working.
  facing = 1; //This dictates the way the player is facing
  minMon = 2; //This is for at new system that spawns monsters.
}

function draw() {
  background (100);
  frame = frameCount; //Starts framecounter
  instructions();
  newMaze(); //Creates the maze
  monsterSetup(minMon); //Monsters
  player(); //Displays the player and controls
  monLogic(); //Deals with everything related from behavior to displaying
  firebolts(); //The Logic of the fireballs
  //I could possibly have written these as a single function
  collsion(); //Calculates if collisions are happening to the player
  monCollide(); //Calculates if collsionis happening to the monsters
  newMonster(); //Generates new monsters when player is in the maze
  end(); //This makes the game able to be won or lost.

  //For testing
  // console.log(frame);
  // console.log(time);
}

function instructions() {
  //This displays the instructions.
  push()
  fill(0);
  textSize(12);
  textAlign(LEFT,CENTER);
  text('Controls: ARROWS or WASD',5,10);
  text('Objective: Get through the maze.',5,30);
  text('More monsters will appear over time.',5,50);
  text('Pressing the left mousebutton fires a fireball',5,70);
  pop()
}
function newMaze() { //v4
  //I have made use of the 10Print formular to make my walls.
  //I add all new walls to an array so I can use them for calculations.
  //I ended up going through 4 different itterations of this part alone.
  if(wYPos <= 500) {
    walls[wIndex] = new Wall(wXPos,wYPos,wWidth[wIndex]);
    wXPos += wWidth[wIndex] + 30;
    wIndex++; //Chinging the index makes it so the walls keeps getting added.
    for(let i = 0; i < walls.length; i++) {
      if(wXPos > 650) {
        wXPos = 0;
        wYPos += 50;
    }
  }
}
for(let w of walls) {
  w.show();
}
}
function player() {
  //This is all part of the Entity object class. That is in another folder.
  hero.show(heroImg[facing],2);
  hero.border();
  hero.run(topSide,bottomSide,leftSide,rightSide); //I add these arguments to the class.
}
function monLogic() {
  //Similar to the player function it is just applied to any monster, that is generated
  for(m of monsters) {
    m.show(monImg,2);
    m.border();
    // The monsters behavior is split into two. I tried something more complex
    // But it did not work.
    if(hero.pos.y > 120) {
      m.chace(hero.pos.x,hero.pos.y,topMon,bottomMon,rightMon,leftMon)
    } else if(hero.pos.y < 120) {
        m.roaming();
    }
  }
}
function firebolts() {
  //Shows and moves the fireballs
  for(f of fire) {
    f.show();
    f.fly();
  }
  for(let i = 0; i < monsters.length; i++) {
    for(let j = 0; j < fire.length; j++) {
      /*
      I rationalise that if the monsters kill feature is active
      The fireball has hit. This also means that killing and being killed
      is the same.
      I found that it is really important that i splice the index rather than
      the index within the array.
      */
      if(monsters[i].kill(fire[j].x,fire[j].y) == true) {
        fire.splice(j,1);
        monsters.splice(i,1);
        killer = true;//This makes it so the program ends differently
      }
    }
  }
  //This removes the fireballs that exit the screen.
  for(let j = 0; j < fire.length; j++) {
    if(fire[j].x < 0 || fire[j].x > 600) {
      fire.splice(j,1);
    }
  }
}
function newMonster(sRate=7,maxMon=10) {
 if(hero.pos.y > 120) { //Checks if the player is in the maze.
    if(frame % 60 == 0) {
      time++;
    }
    if(time == sRate && monsters.length < maxMon) { //It adds a new monster at interals.
      /*
      My reason for adding monsters is to get the player to hurry the longer
      they wait the harder it will be to get through.
      */
      //I add to the minimum amount of monsters in the maze.
      minMon++;
      time = 0;
    }
  }
}
function end() {
  /*
  This part is so the game is tecnically winable
  I have just assigned a value on the y-axis
  that when reached stops the loop and displays
  a victory message
  */
  if(hero.pos.y === 520 && killer == false) {
    push()
    clear();
      createCanvas(windowWidth,windowHeight);
      textSize(30);
      textAlign(CENTER,CENTER);
      fill(190, 80, 0);
      text('You Escaped',windowWidth/2,windowHeight/3);
    noLoop();
    pop()
  } else if(hero.pos.y === 520 && killer == true) { //If anything is killed
    push()
    clear();
      createCanvas(windowWidth,windowHeight);
      textSize(30);
      textAlign(CENTER,CENTER);
      fill(0);
      text('Are You Really A Hero?',windowWidth/2,windowHeight/3);
    noLoop();
    pop()
  }
  /*
  This part checks if the player and any one monster overlap if yes it Displays
  the loosing message.
  */
  for(m of monsters) {
    if(m.kill(hero.pos.x,hero.pos.y) == true) {
      push()
      clear();
        createCanvas(windowWidth,windowHeight);
        textSize(30);
        textAlign(CENTER,CENTER);
        fill(190, 0, 0);
        text('You Perished',windowWidth/2,windowHeight/3);
      noLoop();
      pop()
    }
  }
}

function collsion() {
  /*
  Most likely the single most complicated aspect of the entire program.
  It checks every wall in the maze and their placement in comparison so the
  player or monster. If the player or monster is inside the dimensions of the
  wall. The two first ifstatements (They can be written as one).

  If the first two are true then the object checks where it is placed
  in comparison to the wall. I.e. wether or not it is over/under,left or right.

  I have chosen to seperate the y and x axis, since it is posiible for both
  to be true at the same time.
  */
  for (let i = 0; i < walls.length ; i++) {
    if(hero.pos.y+r >= walls[i].y && hero.pos.y-r <= walls[i].y + walls[i].h) {
      if(hero.pos.x+r >= walls[i].x && hero.pos.x-r <= walls[i].x+walls[i].w) {
        if(hero.pos.y < walls[i].y) {
          topSide = true;
          hero.pos.y-=2;
          topSide = false;
        } else if (hero.pos.y > walls[i].y+walls[i].h) {
          bottomSide = true;
          hero.pos.y+=2;
          bottomSide = false;
        }
        if (hero.pos.x < walls[i].x) {
          leftSide = true;
          hero.pos.x-=2;
          leftSide = false;
        } else if(hero.pos.x > (walls[i].x+walls[i].w)) {
          rightSide = true;
          hero.pos.x+=2;
          rightSide = false;
        }
      }

    }
  }
}
function monCollide() {
  /*
  This is essitially the same function but written a little differently
  It can be viewed as a second itteration of the collision detection.
  I believe I would be able to write just one, if I were to visit this sketch
  again. Since the player and monster arrays could be written as one entity
  array.
  */
  for(let j = 0; j < monsters.length; j++) {
    for (let i = 0; i < walls.length ; i++) {
      if(monsters[j].pos.y+r >= walls[i].y && monsters[j].pos.y-r <= walls[i].y + walls[i].h
        && monsters[j].pos.x+r >= walls[i].x && monsters[j].pos.x-r <= walls[i].x+walls[i].w) {
          if(monsters[j].pos.y < walls[i].y) {
            topMon = true;
            monsters[j].pos.y = walls[i].y-r;
            topMon = false;
          } else if (monsters[j].pos.y > walls[i].y+walls[i].h) {
            bottomMon = true;
            monsters[j].pos.y = walls[i].y+walls[i].h+r;
            bottomMon = false;
          } else if (monsters[j].pos.x < walls[i].x) {
            leftMon = true;
            monsters[j].pos.x = walls[i].x-r;
            leftMon = false;
          } else if(monsters[j].pos.x > (walls[i].x+walls[i].w)) {
            rightMon = true;
            monsters[j].pos.x = walls[i].x+walls[i].w+r;
            rightMon = false;
          }
        }
      }
  }
  }

function booleanSetup() {
  //I have gathered these booleans to make the program a bit more tidy.
  topSide = false;
  bottomSide = false;
  leftSide = false;
  rightSide = false;
  topMon = false;
  bottomMon = false;
  leftMon = false;
  rightMon = false;
}
function monsterSetup(number=2) {
  //I use a loop to instantly make a number of monsters appear.
  //I have a parameter, that makes the standard number of enemies 2.
  if(monsters.length < number) {
    monsters.push(new Entity(random(50,550),185+(50*floor(random(7))),r));
  } //The monsters appear at random
}
function wallSetup() {
  //I generate a number of random integers that will be used to randomise the walls.
  for(let i = 0; i < 40; i++) {
    wWidth[i] = int(random(50,400));
  }
  //I also state the initial state of the walls position.
  wIndex = 0;
  wXPos = 0;
  wYPos = 100;
}
function mouseClicked() {
  //When the mouse button is pressed the game makes a fireball.
  if(mouseButton == LEFT) {
    if(fire.length < 2) { //There cannot be more than two fireballs at a time.
      if(facing == 1) {
        fire.push(new Firebolt(hero.pos.x,hero.pos.y,5,1));
      } else if(facing == 0) {
        fire.push(new Firebolt(hero.pos.x,hero.pos.y,5,0));
      }
    }
  }
}
