## Note
The sprites used in this game was NOT created by me. I am only able to place
the origin of the 'Hero' sprite as a sprite from the game
*Fire Emblem Awakening*.

Furthermore this is a version 2 of this program, for version 1 as well as an expansion of my thoughts on this project follow this link:

Version 1 of Program: https://gitlab.com/Vinmek/aesthetic-programming/-/tree/main/miniX06
### Links
My RUNME: https://vinmek.gitlab.io/aesthetic-programming/miniX07/IndexX7

My Code Files:

Main - https://gitlab.com/Vinmek/aesthetic-programming/-/blob/main/miniX07/miniX7.js

Entity - https://gitlab.com/Vinmek/aesthetic-programming/-/blob/main/miniX07/miniX7_Entity.js

Fireball - https://gitlab.com/Vinmek/aesthetic-programming/-/blob/main/miniX07/miniX7_Fire.js

Object - https://gitlab.com/Vinmek/aesthetic-programming/-/blob/main/miniX07/miniX7_Object.js


## Revisiting past works miniX7
I have chosen to add to my minix6. Though the work was not bad or
underdeveloped actually I would say the miniX6 was one of my best works. I
nonetheless decided to continue my work with it.

This comes from me wanting to explore some of the possibilities I ended up
presenting in my previous README. The intent is therefore not one of revision,
but rather one of addition.
#### Recapping the program
I will start by quickly describe the idea behind my program once more.
My program is a short game, where the objective for the player is to get from
one end of the maze to the other, while dodging skeletons, that begin to chase
you, once you enter "their" area. The program uses to types of objects one
is the walls of the maze and the other is the living entities that inhabit
or attempts to pass through it.
#### My Improvements
I have made some very simple improvements to the game. One being that the
characters display the direction they're facing and the skeletons not moving
at the exact same speed.

The one major conceptual difference between this work, and the one before, is
the inclusion of a new object the firebolt and the players ability to make use
of them. Through these fireballs the player is now able to *kill* just like
the computer is. Further blurring the lines between what is considered a
monster or hero in the universe of the game.

![](Gifs/Gif_miniX7_1.gif)

### To kill or not to kill
This new reality I have made is one of the two ideas I presented in my previous
miniX. I had to choose between taking the monsters ability to kill away from
them or give the player the ability to kill as well.
I ended up going with giving the player the ability to kill, because this
presented the better conceptual world. Taking the ability to kill away from the
skeletons would have brought some interesting concepts, but not as much as
giving the player a choice.

I wanted to give the player a choice. Because by doing so the player is not
forced into the role of the hero. It is not a given from the start. If it ever
were at all.

When working on this addition I realised that the player is far from acting
heroic in the first place. The *Hero* is by all meanings of the word
trespassing on the territory of the skeletons who are trying to defend it.
Even in my base game the skeletons will ignore you, even when being right
beside you, as long as you didn't encroach on their domain.

![](Gifs/Gif_miniX7_2.gif)

By giving the player the ability to not only trespass, but also directly harm
the skeletons. The hero facade starts to slip. It is easier to act with
violence, but can you call yourself the hero if you do?

This is my big question with this work. And what I wanted to make more clear.
Because isn't the role we play dictated by our actions? Or are you a hero as
long as you win? It might be a case of does the end justify the means?
I don't believe that I have an answer to the question. I have given the player
a choice now, and I believe it is up to them to decide if they would still call
themselves a hero, if they kill the skeletons.

![](Gifs/Gif_miniX7_3.gif)
