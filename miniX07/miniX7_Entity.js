class Entity {
  //The constructor is translating out of Class information to in Class information
  constructor(_x, _y, _r) {
    this.pos = createVector(_x,_y)
    this.r = _r;
    this.speed = random(0.7,0.94);
  }
  show(img,sizeMod) {
    //Makes the Entity display images. The size modifier is to make it easier
    // to chage the size of the sprites.
    imageMode(CENTER);
    image(img,this.pos.x,this.pos.y,sizeMod*r,sizeMod*r);
  }
  run(top,bottom,left,right) {
    /*
    This gives the entity the ability to run and is the defining
    feature of being a Hero. These features are controlled by the collision
    function. There are two different ways to control.
    */
    if(top == false) {
      if(keyIsDown(83) || keyIsDown(40)){ //83 is the code for the s key. 40 is Down
        this.pos.y++;
      }
    }
    if(bottom == false) {
      if(keyIsDown(87) || keyIsDown(38)){ //87 is the code for the w key. 38 is Up
        this.pos.y--;
      }
    }
    if(right == false) {
      if(keyIsDown(65) || keyIsDown(37)){ //65 is the code for the a key. 37 is Left
        this.pos.x--;
        facing = 1;
      }
    }
    if (left == false) {
      if(keyIsDown(68) || keyIsDown(39)){ //68 is the code for the d key. 39 is Right
        this.pos.x++;
        facing = 0;
      }
    }
  }

  border() {
    /*
    This is pretty simple
    I check if the placement of an Entity
    is beyond the screen border I reassign it
    so that it ends up back on the canvas
    on the other side.
    */
    if(this.pos.x < 0) {
      this.pos.x = 600;
    }
    if (this.pos.x > 600) {
      this.pos.x = 0;
    }
    if(this.pos.y < 8) {
      this.pos.y = 8;
    }
    if (this.pos.y > 592) {
      this.pos.y = 592;
    }
  }
  chace(otherX,otherY,top,bottom,left,right) {
    /*
    This is the tracking ability, that is the defining feature of being a
    "monster". The program checks if the entity is above or below the player.
    And moves accordingly.
    */
    let dY = this.pos.y - otherY;
    let dX = this.pos.x - otherX;
    if(topMon == false) {
      if (dY < 0) {
        this.pos.y+=this.speed;
      }
    }
    if(bottomMon == false) {
      if(dY > 0) {
        this.pos.y-=this.speed;
      }
    }
    if(rightMon == false) {
      if(dX > 0) {
        this.pos.x-=this.speed;
      }
    }
    if (leftMon == false) {
      if (dX < 0) {
        this.pos.x+=this.speed;
      }
    }
  }
  roaming() {
      //The neutral state of the monsters. Makes them walk endlessly
      this.pos.x += this.speed;

  }
  kill(otherX,otherY) {
    /*
    Calculates the distance between two objects gives the monsters the
    ability to kill or be killed.
    */
    let d = dist(otherX,otherY,this.pos.x,this.pos.y);
    if(d < r) {
      return true;
    }
  }
}
