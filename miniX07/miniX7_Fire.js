class Firebolt {
  constructor(_x, _y, _r,_way) {
    this.x = _x;
    this.y = _y;
    this.r = _r;
    this.way = _way;
    this.speed = 2;
  }
  show() { //Shows the fireballs.
    push()
    fill(250,50,0);
    ellipse(this.x,this.y,this.r);
    pop()
  }
  fly() { //Makes the fireballs move. Based on the way the player faces.
  if(this.way == 0) {
    this.x+=this.speed;
  } else if(this.way == 1) {
    this.x-=this.speed;
    }
  }
}
