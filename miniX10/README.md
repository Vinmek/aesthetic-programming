## Chosen ml5 AI: Pix2Pix <br>
<img src="Assets/pikachu_example.png" alt="Pikachu example" width="400"/>


**Which sample code have you chosen and why?** <br>
We chose Pix2Pix, because it seems fun. We found it cool that the program tries to color in the pictures/drawings you upload. It colors based on a set picture of the Pokémon Pikachu and then applies those colors and style to the picture the user has uploaded.

**Have you changed anything in order to understand the program? If yes, what are they?** <br>
We tried changing the images, because we thought the program would try to color them accordingly - we found out that the program, no matter the image, will try to color in Pikachu. All images will have a big splash of yellow and spots of other colors. When clicking around, we found that you don’t need a picture. There is a drawing function, making it so that you can try and draw your own Pikachu for the program to colour. <br>
<img src="Assets/1.png" alt="Pikachu" width="199"/>
<img src="Assets/2.png" alt="Pikachu" width="200"/>
<img src="Assets/9.png" alt="Blastoise" width="293"/> <br>
In order to better understand the program's inner workings we ended up changing the status message text so that the program would show when an error occurred.

**Which line(s) of code is particularly interesting to your group, and why?** <br>
We were puzzled by the transfer function for a while, because we thought the transferring would generate a new image (this was prior to discovering that you could draw). We were confused, because we couldn’t make the drawing function work, and we don’t know if it’s because our laptops can’t handle the program, but it is not working.
An example from a site where it worked by Yining Shi: <br>
<img src="Assets/angry_pikachu.png" alt="Pikachu" width="600"/>  <br>


These two lines of code below display and update the program status on the webpage. The thing that makes this interesting to us, is that it is the first time we have seen the HTML being manipulated from within the .js file. We even ended up using the second line to troubleshoot in the program.

    <p id="status">Loading Model... Please wait...</p>
    statusMsg.html('Model Loaded!');

This other function puzzled us tremendously at the beginning, but helped us realize the true workings of the program. Though our computers seemed to be unable to run this function properly, we still find it to be interesting. This function is supposed to auto update the drawn image to have the program coloring the sketch, so long as the program isn’t already transferring (updating).

    function mouseReleased() {
      if (modelReady && !isTransfering) {
        transfer()
      }
    }


**How would you explore/exploit the limitation of the sample code/machine learning algorithms?** <br>
We found that pix2pix has many limitations, at least the version we are working with currently. We found ways of testing the limits of the AI by uploading different images and changing the pixel density. We found that some of the problems were the weight of the program. Our hardware couldn’t really handle the running of the program, if values were raised too much. We found that raising the pixel density in some cases improved the look of pictures and in others made the colors variation disappear. <br>
<img src="Assets/3.png" alt="Flower" width="192"/>
<img src="Assets/4.png" alt="FlowerXL" width="200"/>
<img src="Assets/8.png" alt="Lizard" width="293"/> <br>
Colors especially ended up being extremely interesting, because a picture without any color in the program would just produce a fully colored picture, while a already colored picture in some places will gain a stark contrast between fore and background.<br>
<img src="Assets/7.png" alt="Will" width="200"/>
<img src="Assets/5.png" alt="Catch" width="228"/>
<img src="Assets/6.png" alt="Bulbasaur" width="200"/> <br>
**What are the syntaxes/functions that you don't know before? What are they and what have you learnt?** <br>
We are not as familiar with HTML as JavaScript, but we found it relatively easy to read the code. There was however one syntax line that was fairly unfamiliar to us. That being the
inputCanvas.class('border-box').parent('canvasContainer');
inputCanvas is here a variable, that is defined as a canvas only 256 * 256 in size.

Border-box seems to be a css syntax that styles the constraints of the canvas. The parent syntax seems to be used to attach the element to a parent. In this case this syntax is what transfers the canvas into the HTML code as canvasContainer is also referred to here.

**How do you see a border relation between the code that you are tinkering with and the machine learning applications in the world (e.g creative AI/ voice assistants/driving cars, bot assistants, facial/object recognition, etc)?** <br>
It is a very exploratory setting where you experiment in an ongoing stage. For example, with the code we have looked at with Pix2Pix, we could recognize patterns and behaviors in the program that had great influence on the output.
In the “real world” these scenarios are seen often as examples in games where the AI is a type of enemy or combatten. Another example is Snapchat, with its insanely good AI in facial recognition. An AI that is similar to Pix2Pix is autocorrect, that tries to fill in what you wanted, based on input from you that may contain typos (or a drawing that might not look like the official art).


**What do you want to know more/ What questions can you ask further?** <br>
We would like to learn how the program sometimes displays other colors that are not in the original Pikachu picture. We discussed that the blue color could be from pikachu's mouth, that may have some small traces of purple and blue color, and maybe that's why it uses those colors sometimes.<br>

Also, what dictates the color aspects in the program? And how can it differentiate between the different lines and color the right spaces with little to no failure?

-------

References: <br>
Yining Shi Site: http://1023.io/pix2pix-edges-to-pikachu-2 <br>
ml5.js Pix2Pix: https://learn.ml5js.org/#/reference/pix2pix <br>
