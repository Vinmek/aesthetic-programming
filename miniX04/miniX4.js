/*
I am so sorry for what you are about to see
I don't know what I was thinking.
Well we start with the usual asortment of variables and arrays.
*/
let w_width; let w_height;
let count;
let delta; let delta2; //Two timers I might only need one, but just in case.
let fading; //The text fading effect.
let r_w; let r_w2; //Index number for the displaing of text.
let pause1;

let greeting = [];let words = [];let why = [];let ret = [];//Text arrays

let button1; let button2; //Creating the buttons.
let name; //Makes it so I can make a input field.
let count_like; let count_dislike;//Sets up counter.

let sum_like; let sum_dislike; //Sets up variable for data recall.

let bootYr; let bootMth; let bootD; //For collecting date.
let bootHr; let bootMin; let bootSec; //For collecting time.

let flag; //Basically the backbone of the program.

let tool; let toolX; let toolY; //All about the hammer
let ham_dist; //Distant to the hammer.
let shut; //The shutdown
let testorigin; //Testing

function preload () {
  //Preloads the image of the hammer
  tool = loadImage('Hammer_miniX4.png');
}
function setup() {
  w_width = windowWidth; //Sets width
  w_height = windowHeight; //Sets height
  //These three controls the timing of the text.
  delta = 0;
  delta2 = 0;
  fading = 0;
  //These determin how the text is displayed.
  r_w = int(random(words.length));
  r_w2 = 0;
  pause1 = false; //Pauses the program
  //Initial placement of hammer.
  toolX = w_width;
  toolY = w_height - (w_height/12);
  shut = 1;
  //These two will be explained, but it's to log the time of bootup.
  timeBootup();
  likeBootup();
  //These two checks if the program has been activated before.
  //I could probably have had only one.
  welcome = getItem('Return');
  broken = getItem('Return2');
  your_name = getItem('Name');
  name = createInput('');
  //This it what checks if the program has been active.
  revisit();
  buttons(); //Makes buttons show.
  createCanvas(w_width, w_height);
}
function draw() {
  //The draw function is disturbingly empty now that I think about it.
  background(255,168,171); //Creates the pink area.
  count = frameCount; //Uses this to calculate the time.
  bg_test(); //This creates the white area outside the pink one.
  translate(w_width/2, w_height/3); //Changes the point of origin.
  program_state(); //The brains of the program/story.
  konsol(); //Just for testing purposes.
}

function windowResized() {
  //Pretty useless just resizes the canvas.
  resizeCanvas(w_width, w_height);
}

function timeBootup() {
  //Stores the values of the computers clock.
  storeItem('yr',year());
  storeItem('mth',month());
  storeItem('d',day());
  storeItem('hr',hour());
  storeItem('min',minute());
  storeItem('sec',second());
}
function likeBootup() {
  //Makes it so the like counter doesn't reset every time the program starts.
  sum_like = getItem('nr_like');
  sum_dislike = getItem('nr_dislike');

  if(sum_like == null) { //Checks if a number is already stored.
    count_like = 0; //If not the value is set to 0.
  } else {
    count_like = sum_like;
  }
  if(sum_dislike == null) { //The same just for dislikes.
    count_dislike = 0;
  } else {
    count_dislike = sum_dislike;
  }
}
function bg_test() {
  //Creates four white squares to make an illusion of an out of bounds.
  fill(255);
  noStroke();
  rect(0,0,w_width,w_height/6);
  rect(0,w_height-(w_height/6),w_width,w_height/6);
  rect(0,0,w_width/8,w_height);
  rect(w_width-(w_width/8),0,w_width/8,w_height);
}
function program_state() {
  //This is the progression of the program that is controlled by flags.
  //An old gem I have already told about on the class.
  if(flag == 10 || flag == 11) {
  reboot();
} else if(flag < 6) {
  intro();
} else if(flag == 6 || flag == 12) {
  test();
} else if(flag == 13) {
  shutdown();
} else if(flag == 14) {
  backend();
}
}
function intro() {
  wordsalad(); //Gets the phrases for the intro.
  push();
  //Displays the intro text. Controlled by flag.
  textAlign(CENTER,CENTER);
  fill(0, fading);
  textSize(40);
  text(greeting[flag], 0, 0); //The flag represents the indexvalue.
  text_change(); //Will be explained later.
  if (fading == 240 && flag == 2) { //Pauses the program and displays the input.
    pause1 = true;
    name.center();
    name.size(150);
    keyPressed(); //Will be explained.
    pop();
  }
}
function reboot() {
  wordsalad();
  push();
  textAlign(CENTER,CENTER);
  fill(0, fading);
  textSize(40);
  textWrap(WORD);
  text(ret[flag-10], 0, 0);
  text_change();
  pop()
}
function text_change() {
  //This controls the fading effect on the text.
  /*
  Counts the frames since start. The % is the remainder.
  So the idea is if framecount is 59 the remainder is 1.
  This means that when framerate is 60 the remainder is 0.
  Since the program runs 60 frames a second I now have a clock
  that counts up every second.
  */
  if(count % 60 == 0) {
    delta++;
  }
  /*
  Shifting between adding to the fading og removing it, as long as pause
  is not active. About the fading I am aware that the fading is slow.
  I have tried to increase it, but then the timing goes off so I have not.
  */
  if(pause1 == false) {
    if(delta > 2 && delta < 7) {
    fading++;
  }
  if (delta >= 7 && fading > 0) {
    fading--;
  }
  if (fading == 0 && delta >= 10) {
    flag++;
    delta = 0;
    }
  }
}
function keyPressed() {
  //When the enter key is pressed once the input is active.
  //The program records what was in the input field.
  if(keyCode === 13 && flag == 2) {
    storeItem("Name",name.value());
    /*
    This following part is important, because it changes the bootup
    of the program. Will go into detail in revisit().
    */
    storeItem("Return", 1);
    removeElements(); //Removes the elements.
    buttons(); //Re adds the buttons as they are also removed.
    pause1 = false; //Restarts the text_change.
  }
}
function revisit() {
  //Chenges the start of the program. If the welcome is not a null value.
  if(welcome != null && broken == null) { //Null value means nothing is stored.
    flag = 10;
  } else if(welcome != null && broken != null) { //This is for the last bootup.
    flag = 14;
  } else {
    flag = 0;
  }
}
function test() {
  wordsalad(); //Finds the words.
  push();
  /*
  Creates texts and places buttons.
  */
  fill(0);
  textAlign(CENTER,CENTER);
  textSize(40);
  text(words[r_w], 0, 0);
  button1.position((w_width/2)-140,(w_height/2));
  button1.size(100,50);
  button1.mousePressed(like); //Defines the effect of Like Button
  button2.position((w_width/2)+40,(w_height/2));
  button2.size(100,50);
  button2.mousePressed(dislike); //Defines the effect of Dislike Button
  pop()

  hammerinteract(); //Moves the hammer
  hammer(); //Displays the hammer
  leaving(); //Changes if you are outside
  //This following checks to see is you are holding the hammer
  //And if you bring it inside making leaving false the program shifts
  //To Shutdown.
  if(hammerinteract() == true && leaving() != true) {
    flag = 13;
  }
}
function buttons() {
  //Makes two buttons.
  button1 = createButton("Like");
  button2 = createButton("Dislike");
}
function like() {
  count_like++ //Increases like count by one
  storeItem("nr_like",count_like); //Stores new like value
  r_w = int(random(words.length)); //Generates new word
}
function dislike() {
  count_dislike++ //Increases dislike count by one
  storeItem("nr_dislike",count_dislike); //Stores new dislike value
  r_w = int(random(words.length)); //Generates new word
}
function leaving() {
  wordsalad(); //Gets phrases
  //This part checks if the mouse is outside the pink Area. MATH!!!!
  if(
  mouseY < w_height/6 || mouseY > w_height-(w_height/6)
  ||
  mouseX < w_width/8 || mouseX > w_width-(w_width/8)
){
  your_name = getItem('Name'); //Just making sure I have the user name ready.
  //Making the text.
  push();
  //Makes a covering pink rectangle
  translate(-w_width/2, -w_height/3); //Retranslate
  fill(255,168,171);
  rect(w_width/8,w_height/6,w_width-(2*(w_width/8)),w_height-(2*(w_height/6)));
  pop()
  why_text(); //Shows text
  return true; //Returns true to the if statement
}
}
function why_text() {
  push();
  //Displays question text
  fill(0);
  textSize(40);
  textAlign(CENTER,CENTER);
  text(why[0 + r_w2], 0, 0);
  //New clock works the same as the old clock.
  if(count % 60 == 0) {
    delta2++;
  }
  if(delta2 > 5) { //Changes the text every 5 seconds.
    r_w2 = int(random(why.length));
    delta2 = 0;
  }
  pop();
}
function hammer() {
  push();
  translate(-w_width/2, -w_height/3) //Retranslates because it conflicts with
                                    //mouse coordinates if origin is changed
  imageMode(CENTER);
  image(tool,toolX,toolY,100,100); //Makes image

  //Calculates distance between mouse and hammer.
  ham_dist = dist(mouseX,mouseY,toolX,toolY);
  pop();
}
function hammerinteract() {
  if(ham_dist < 50) { //If the mouse is over the hammer.
    if(mouseIsPressed) { //And the mouse is pressed.
      //The hammer follows the mouse.
      toolX = mouseX;
      toolY = mouseY;
      return true; //Returns true to the If statement.
    } else {
      //And it returns to the original place when let go.
      toolX = w_width;
      toolY = w_height - (w_height/12);
    }
  }
}
function shutdown() {
  //Shutdown clears the canvas.
  clear();
  removeElements(); //Elements separetely
  background(0); //Draws new background.
  push();
  translate(-w_width/2, -w_height/3) //Retranslates
  //Creates the blue screen effect.
  noStroke();
  fill(0,0,255);
  rect(0,0,20*shut,10*shut)
  pop();
  if(shut < 32768) { //This part makes the it happen gradually.
    shut = shut*2;
  }
  if(shut == 32768) {
    flag = 14; //Moves the program on.
    storeItem("Return2", 1); //Makes the program boot up on bluescreen
  }
}
function backend() {
  timeGet(); //Gets the time from storage
  //Gets the number of likes & dislikes
  sum_like = getItem('nr_like');
  sum_dislike = getItem('nr_dislike');
push();
  translate(-w_width/2, -w_height/3) //Retranslates
  //Displays the stats collected through the use of the program.
  background(0,0,255);
  fill(255);
  textAlign(LEFT);
  textSize(20);
  text("Name: " + your_name,10,20); //Uses your name
  text("Last Bootup: " + bootHr+":"+bootMin+":"+bootSec+" | "
    +bootD+"-"+bootMth+"-"+bootYr,10,40); //Uses the time
  text("Number of likes: " + sum_like,10,60); //Uses number of likes
  text("Number of dislikes: " + sum_dislike,10,80); //Numner of dislikes.
  // testorigin = dist(mouseX,mouseY,0,0); //For Testing
  if(keyCode == 81) { //The only way for others to reset. As far as I know.
    clearStorage();
    }
  pop();
}
function timeGet() {
  //Gets times from storage.
  bootYr = getItem('yr');
  bootMth = getItem('mth');
  bootD = getItem('d');
  bootHr = getItem('hr');
  bootMin = getItem('min');
  bootSec = getItem('sec');
}
function wordsalad() {
  //This is where all the word strings are stored.
  //The text from the first bootup.
  greeting = [
  "Welcome",
  "Shall We Begin?",
  "Please Tell Me Your Name",
  "Excellent",
  "Now. Please tell me.",
  "What you think of this word"
  ]

  //Text from the revisit part.
  ret = ["Welcome Back " + your_name, "Shall We Continue?"]

  //The possible words that the program can ask you to value.
  //Could have done this better.
  words = [
    "Nervous",
    "Folk",
    "Separation",
    "Plastic",
    "Hell",
    "Activate",
    "Formulate",
    "Body",
    "Steel",
    "Cabin",
    "Negligence",
    "Qualified",
    "Tactic",
    "Column",
    "Wording",
    "Revenge",
    "Objective",
    "Display",
    "Safety",
    "Sector",
    "Exile",
    "Charge",
    "Observation",
    "Convention",
    "Express",
    "Catalogue",
    "Movie",
    "Queue",
    "Tax",
    "Flat",
    "Base",
    "Tap",
    "Competition",
    "Committee",
    "Workshop",
    "Acceptance",
    "Palace",
    "Gas",
    "Evening",
    "Pig",
    "Brush",
    "Collapse",
    "Different",
    "Stage",
    "Bomber",
    "Option",
    "Faint",
    "Sting",
    "Treat",
    "Depressed"
  ];

  //The reactions to going out of bounds.
  why = [
    "Where are you going?",
    "Please come back",
    "There is nothing out there",
    "We cannot continue unless you return",
    "What are you doing?",
    "There is no point to stay out there",
    "...",
    "......",
    "Do you not wish to continue?",
    "Is there a problem?",
    "This makes things dificult for me",
    "You being out there is not helping",
    your_name + "?",
    your_name + " can you hear me?",
    "Please return to the test area",
    "Are you ignoring me?"
  ];
}

function konsol() {
  // console.log(flag);
  // console.log(delta);
  // console.log(delta2);
  // console.log(fading);
  // console.log(ham_dist);
  // console.log(shut);
  // console.log(testorigin);
  // console.log(sum_like);
  // console.log(sum_dislike);
}
