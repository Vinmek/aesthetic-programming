### Links and short guide.
Link to RUNME: https://vinmek.gitlab.io/aesthetic-programming/miniX04/indexX4

Link to Code: https://gitlab.com/Vinmek/aesthetic-programming/-/blob/main/miniX04/miniX4.js

#### Important
Pressing enter after writing the name will make the program
progress. The programs full functions is best shown through
a reload of the browser or page. The program can generally not
be reset, but if you get to the end. Pressing Q will allow for a
total reset.

## Welcome back "MiniX4"
Welcome back is an interactive work. Centered around data capture
the idea behind the work is the relationship between the human and
the machine during the process of gathering data.

Throughout the work the program is requesting information from the
user trying to gain more and more information. The program also
remembers the users name and last time it was booted up. Though
non of this information is being presented willingly to the user of
the program. The user do not get any information about what is going
on backend which is the point. I wanted to tackle the relationship
and a relationship goes both ways. It is true that machines all
around us look at our actions and interpret what we do.
But isn't that also true the other way around? We look at programs
and try to read what they see, what they know problem is, we are
not as good at spotting these things as the computer is. And in the
end our frustrations might have us brute force our way to answers
if that is even possible.
### My program
The idea of my program would work best as a handout or a distributed
link instead of something that is installed.

The program is build around the Item syntaxes, that allow the
storing and recalling of data from the browsers local storage. This
makes it possible to remember certain types of data.

![](Billede_miniX4_1.png)

The possibilities are endless, but I have chosen to focus on only a
few data points that are for the most part gathered as a natural
part of the functioning of the program. When running the number of
times the user presses a button a hidden tally on the backend
increases. The program also acts as if it is alive. An effect I have
provoked by letting it react to certain actions, such as the user
removing the cursor from the "test area".

Most of the program progresses through a short narrative. With the
program wanting to learn form the user asking for their name and
presenting words to illicit responses. If the user moves outside
the boundaries of the test, the program tries to persuade them back
in.

![](Billede_miniX4_2.png)

![](Billede_miniX4_3.png)

The user can quit the program and return and the program will
welcome them back. At the "end" of the narrative the user or
"test subject" smashes the program with a hidden hammer which
finally reveals the backend data, that has been collected.

As stated most of the program is controlled through use of stored
items (storeItem & getItem), I could probably have stored all kinds
of data through these functions, how many times the program have
been booted up, where the mouse cursor have been placed most, how
many times the user left the test site the list is endless. The
main difficulty was time constraints. To be honest I have basically
made a short game here, with a story line and some sort of
progression. I had some things I had planned to add such as an API
that could collect random Words to feed the questions, I ended up
doing it the way I did to "simulate" how the program would be
working. I had also had plans to ad audio recording as well as
video. The reason for this is to add to the paranoia of knowing
data is being collected, but not knowing what it is being used for.
I would not have used the data gathered for anything the pop up to
allow for a recording would be the main point of adding it.

### On Data Capture
I believe that there is truth to the fact that data capture is
both the infrastructure as well as value generation. As Ulises A.
Mejias states in the text *Datafication* But I think one more factor
applies and that is knowledge. Maybe understanding is a better word.
As stated above data capture is something that goes both ways.
Gathering data has been a thing since way before the computer. The
thing that has changed is in my opinion the amount of data being
gathered and the fact that the data is being analyzed and used in
real time. And the fact that we are being made aware of the effects.
Most people (maybe all people) live in a valley where they know
that data is being gathered, and they know that something is being
done with that data. How do they know? They see it all over. From
adds to notifications. But what they do not know is. Who is looking.
Where is the listeners and how is the data being interpreted.

It is here my program and my concept of understanding comes in.
I think my program will affect different people differently.
My sister got really stressed out by the program because it was
in her words "Demanding me to keep answering". With a higher
understanding of programs she would maybe have been able to
interpret some of the stuff going on back end.

Ed Finn writes in *What Algorithms Want* about how algorithms are
worshipped in a sense because people do not understand what they do.
I believe it to be true, that one problem with data capture is our
lack of understanding. And that if we were to understand what was
going on the idea of data capture would not be as scary because we
would have the knowledge to avoid or choose what parts of our data,
we'd wish to show.
